package ras15.example.convertcurrency;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import ras15.example.convertcurreny.R;

public class MainActivity extends AppCompatActivity {

    double dollar_to_euro = .88;

    double dollar_to_peso = 19.14;

    double dollar_to_canadian = 1.32;

    double dollars_entered;

    double converted_amount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final EditText dollar = (EditText) findViewById(R.id.dollar_amt_entry);
        final RadioButton radbttntoeuros = (RadioButton) findViewById(R.id.radbttntoeuros);
        final RadioButton radbttntopesos = (RadioButton) findViewById(R.id.radbttntopesos);
        final RadioButton radbttntocanadian = (RadioButton) findViewById(R.id.radbttntocanadian);
        final TextView result = (TextView) findViewById(R.id.result);
        Button bttnconvert =(Button)findViewById(R.id.bttnconvert);


        bttnconvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dollars_entered = Double.parseDouble(dollar.getText().toString());
                DecimalFormat tenth = new DecimalFormat("#.#");
                if (radbttntoeuros.isChecked()) {
                    if (dollars_entered <= 100000 && dollars_entered > 0) {
                        converted_amount = (dollars_entered * dollar_to_euro);
                        result.setText("€" + (tenth.format(converted_amount) + " Euros"));
                    } else {
                        Toast.makeText(MainActivity.this, "US Dollars must be <= 100000 and > 0", Toast.LENGTH_LONG).show();

                            }
                }


                if (radbttntopesos.isChecked()) {
                    if (dollars_entered <= 100000 && dollars_entered > 0) {
                        converted_amount = dollars_entered * dollar_to_peso;
                        result.setText("$" + (tenth.format(converted_amount) + " Pesos"));
                    } else {
                        Toast.makeText(MainActivity.this, "US Dollars must be <= 100000 and > 0", Toast.LENGTH_LONG).show();
                    }
                }

                if (radbttntocanadian.isChecked()) {
                    if (dollars_entered <= 100000 && dollars_entered > 0) {
                        converted_amount = dollars_entered * dollar_to_canadian;
                        result.setText("$" + (tenth.format(converted_amount) + " Canadian"));
                    } else {
                        Toast.makeText(MainActivity.this, "US Dollars must be <= 100000 and > 0", Toast.LENGTH_LONG).show();
                    }
                }

            }

    });
}
}