# LIS4331 - Advanced Mobile Applications Development

## Remy Santos

### Assignment #3 Requirements:

*Deliverables:*

1. Completed Android Currency Converter App
2. Launcher Icon
3. Screenshots of running app


#### README.md file should include the following items:

* Screenshot of running application�s unpopulated user interface.
* Screenshot of running application�s toast notification
* Screenshot of running application�s converted currency user interface
* Provide Bitbucket read-only access to repo

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of Unpopulated Interface - Convert Currency App*: 

![Unpopulated Convert Currency](https://bitbucket.org/Ras15/lis4331/raw/37566161c368b2a56a0a38a2a6e84ebd7ba28633/a3/img/Unpopulated.png)

*Screenshot of Toast Notification - Convert Currency App*:

![Toast Notification Example](https://bitbucket.org/Ras15/lis4331/raw/37566161c368b2a56a0a38a2a6e84ebd7ba28633/a3/img/Toast_Notification.png)

*Screenshot of Populated Euro Conversion - Convert Currency App*:

![Android Studio Installation Screenshot](https://bitbucket.org/Ras15/lis4331/raw/37566161c368b2a56a0a38a2a6e84ebd7ba28633/a3/img/US_Dollars_to_Euro.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
