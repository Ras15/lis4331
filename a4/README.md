# LIS4331 - Advanced Mobile Applications Development

## Remy Santos

### Assignment #4 Requirements:

*Sub-Heading:*

1. Android Mortgage Interest Calculator
2. Provide Screenshots
3. Provide Git commands

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1
* Screenshot of running application�s splash screen
* Screenshot of running application�s invalid screen (with appropriate image)
* Screenshots of running application�s valid screen (with appropriate image)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of Splash Screen*: 

![A4 Splash Screen](https://bitbucket.org/Ras15/lis4331/raw/666ed25f0f5f663ed7f9a79efe6a7ec059b8d7af/a4/img/SplashScreenA4.png)


![A4 Failed Validation](https://bitbucket.org/Ras15/lis4331/raw/666ed25f0f5f663ed7f9a79efe6a7ec059b8d7af/a4/img/FailedValidationA4.png)

*Screenshot of Passed Validation*:

![A4 Passed Validation](https://bitbucket.org/Ras15/lis4331/raw/666ed25f0f5f663ed7f9a79efe6a7ec059b8d7af/a4/img/PassedValidationA4.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
