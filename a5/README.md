# LIS4331 - Advanced Mobile Applications Development

## Remy Santos

### Assignment #5 Requirements:

*Sub-Heading:*

1. Create Android News Reader RSS Feed Application
2. Provide Screenshots of application running
3. Provide Git command descriptions

#### README.md file should include the following items:

* Screenshot of running application’s splash screen(list of articles –activity_items.xml)
* Screenshot of running application’s individual article(activity_item.xml)
* Screenshots of running application’s default browser(article link)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of running ItemsActivity*: 

![ItemsActivity](https://bitbucket.org/Ras15/lis4331/raw/4d9969aa622ca68289a4aef5ee56ba98e1e2da7e/a5/img/ItemsActivity.png)

*Screenshot of running ItemsActivity*:

![ItemActivity](https://bitbucket.org/Ras15/lis4331/raw/4d9969aa622ca68289a4aef5ee56ba98e1e2da7e/a5/img/ItemActivity.png)

*Screenshot of Read More*:

![ReadMore](https://bitbucket.org/Ras15/lis4331/raw/4d9969aa622ca68289a4aef5ee56ba98e1e2da7e/a5/img/ReadMore.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
