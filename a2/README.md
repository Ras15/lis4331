# LIS4331 - Advanced Mobile Applications Development

## Remy Santos

### Assignment #2 Requirements:

*Sub-Heading:*

* Drop-down menu for total number of guests (including yourself): 1 �10
* Drop-down menu for tip percentage (5% increments): 0 �25
* Must add background color(s) or theme(10 pts)
* Must create and displaylauncher icon image(10 pts)

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1.
* Screenshot of running application's unpopulated user interface.
* Screenshot of running application's populated user interface.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of running Split the Bill unpopulated*: 

![Split_the_Bill](https://bitbucket.org/Ras15/lis4331/raw/6bd1f6c13b72bdcc397b3b124451d5d921feb735/a2/img/Splitthebill_unpopulated.png)

*Screenshot of running Split the Bill populated*:

![Split_the_Bill_populated](https://bitbucket.org/Ras15/lis4331/raw/1837d63995fc3d2112211a2dae796c2076253e4f/a2/img/Splitthebill_populated.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
