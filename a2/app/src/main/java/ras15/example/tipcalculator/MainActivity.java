package ras15.example.tipcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    double bill = 0.00;
    double total = 0.00;
    int guest_choice;
    double tip_choice;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText bill_total = (EditText)findViewById(R.id.editTextBill);
        final Spinner num_guests = (Spinner)findViewById(R.id.spinner);
        final Spinner tip_percent = (Spinner)findViewById(R.id.spinner2);


        Button total_cost = (Button)findViewById(R.id.button);
        total_cost.setOnClickListener(new View.OnClickListener() {
            final TextView result = ((TextView)findViewById(R.id.textResult));
            @Override
            public void onClick(View v) {
                bill = Double.parseDouble(bill_total.getText().toString());
                guest_choice = Integer.parseInt(num_guests.getSelectedItem().toString());
                tip_choice = Double.parseDouble(tip_percent.getSelectedItem().toString());
                double tip = (tip_choice/100);
                total = (((tip * bill) + (bill))/guest_choice);
                DecimalFormat currency = new DecimalFormat("$###,###.##");
                result.setText("Cost for each of " + guest_choice + " guests: " + currency.format(total));
            }
        });


    }
}
