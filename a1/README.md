# LIS4331 - Advanced Mobile Applications Development

## Remy Santos

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chapter 1 & 2)
4. Bitbucket repo links a) this assignment and b)the completed tutorials above (bitbucketstationlocations & myteamquotes)


#### README.md file should include the following items: 			

* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* Screenshots of running Android Studio - Contacts App 
* git commands w/short descriptions;


#### Git commands w/short descriptions:
1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of running java Hello*: 

![JavaSS](https://bitbucket.org/Ras15/lis4368/raw/41cf6646960105ed9f2c4b367824dea63c005375/Hello%20World%20Java%20SS.png)


*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](https://bitbucket.org/Ras15/lis4331/raw/b4a3b52df983a32824bb031ddaebdb425bd0c4d3/a1/img/Screen%20Shot%202019-01-17%20at%203.15.18%20PM.png)

![Android Studio Contacts App Screenshot](https://bitbucket.org/Ras15/lis4331/raw/5e39f78ea0b48dd5256428860b3e9c59c12b62ae/a1/img/Screen%20Shot%202019-01-22%20at%205.57.29%20PM.png)

![Android Studio Contacts App Screenshot](https://bitbucket.org/Ras15/lis4331/raw/5e39f78ea0b48dd5256428860b3e9c59c12b62ae/a1/img/Screen%20Shot%202019-01-22%20at%205.57.36%20PM.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Ras15/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
