# LIS4331 - Advanced Mobile Applications Development

## Remy Santos

### Project #1 Requirements:

*Deliverables:*

1. Completed Muisc App
2. Splash Screen
3. Launcher Icon
4. Screenshots of running app
5. Include artists’ images and media.
6. Images and buttons must be vertically and horizontally aligned.
7. Background color(s) or theme
8. Git commands

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application’s splash screen;
* Screenshot of running application’s follow-up screen(with images and buttons);
* Screenshots of running application’s play and pause user interfaces (with images and buttons);


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of running application’s splash screen*: | *Screenshot of running application’s follow-up screen*:
						  :-------------------------:|:-------------------------:
![Splash Screen](https://bitbucket.org/Ras15/lis4331/raw/39a4a0c5c3e49148f0946b5feb3e85609bd9dd30/p1/IMG/Screen%20Shot%202019-03-04%20at%209.51.47%20PM.png)| ![Follow-up Screen](https://bitbucket.org/Ras15/lis4331/raw/39a4a0c5c3e49148f0946b5feb3e85609bd9dd30/p1/IMG/Screen%20Shot%202019-03-04%20at%209.51.53%20PM.png)

*Screenshot of running application’s play user interface*: | *Screenshot of running application’s pause user interface*:
								 :-------------------------:|:-------------------------:
![Play](https://bitbucket.org/Ras15/lis4331/raw/39a4a0c5c3e49148f0946b5feb3e85609bd9dd30/p1/IMG/Screen%20Shot%202019-03-04%20at%209.51.58%20PM.png) | ![Pause](https://bitbucket.org/Ras15/lis4331/raw/39a4a0c5c3e49148f0946b5feb3e85609bd9dd30/p1/IMG/Screen%20Shot%202019-03-04%20at%209.52.19%20PM.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
