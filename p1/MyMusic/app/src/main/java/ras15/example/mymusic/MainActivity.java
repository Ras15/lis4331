package ras15.example.mymusic;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button button1, button2, button3;
    MediaPlayer mpMichaelJackson,mpDrake, mpAsapRocky;
    int playing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = (Button) findViewById(R.id.btnMichaelJackson);
        button2 = (Button) findViewById(R.id.btnDrake);
        button3 = (Button) findViewById(R.id.btnAsapRocky);
        button1.setOnClickListener(bMichaelJackson);
        button2.setOnClickListener(bDrake);
        button3.setOnClickListener(bAsapRocky);
        mpMichaelJackson = new MediaPlayer();
        mpMichaelJackson = MediaPlayer.create(this,R.raw.beatit);
        mpDrake = new MediaPlayer();
        mpDrake = MediaPlayer.create(this,R.raw.hotlinebling);
        mpAsapRocky = new MediaPlayer();
        mpAsapRocky = MediaPlayer.create(this,R.raw.raf);
        playing = 0;


    }

    Button.OnClickListener bMichaelJackson = new Button.OnClickListener(){
            @Override
            public void onClick(View v) {

                switch(playing) {

                    case 0:

                        mpMichaelJackson.start();
                        playing = 1;
                        button1.setText("Pause Michael Jackson");
                        button2.setVisibility(View.INVISIBLE);
                        button3.setVisibility(View.INVISIBLE);
                        break;

                    case 1:
                        mpMichaelJackson.pause();
                        playing = 0;
                        button1.setText("Play Michael Jackson");
                        button2.setVisibility(View.VISIBLE);
                        button3.setVisibility(View.VISIBLE);
                        break;
                }

            }
    };

    Button.OnClickListener bDrake = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {

            switch(playing) {

                case 0:

                    mpDrake.start();
                    playing = 1;
                    button2.setText("Pause Drake");
                    button1.setVisibility(View.INVISIBLE);
                    button3.setVisibility(View.INVISIBLE);
                    break;

                case 1:
                    mpDrake.pause();
                    playing = 0;
                    button2.setText("Play Drake");
                    button1.setVisibility(View.VISIBLE);
                    button3.setVisibility(View.VISIBLE);
                    break;
            }

        }
    };

    Button.OnClickListener bAsapRocky = new Button.OnClickListener(){
        @Override
        public void onClick(View v) {

            switch(playing) {

                case 0:

                    mpAsapRocky.start();
                    playing = 1;
                    button3.setText("Pause A$AP Rocky");
                    button2.setVisibility(View.INVISIBLE);
                    button1.setVisibility(View.INVISIBLE);
                    break;

                case 1:
                    mpAsapRocky.pause();
                    playing = 0;
                    button3.setText("Play A$AP Rocky");
                    button2.setVisibility(View.VISIBLE);
                    button1.setVisibility(View.VISIBLE);
                    break;
            }

        }
    };
}
