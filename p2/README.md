# LIS4331 - Advanced Mobile Applications Development

## Remy Santos

### Project #2 Requirements:

*Sub-Heading:*

1. Include splash screen.
2. Insert at least five sample tasks (see p. 413, and screenshot below).
3. Test database class (see pp. 424, 425)
4. Must add background color(s) or theme
5. Create and display launcher icon image.


#### README.md file should include the following items:

* Completed Task List Android Appplication
* Screenshot of running Application
* Provide Git command w/ short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. One additional git command (git clone) - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of Splash Screen*: | *Screenshot of running Task List Android Application*:
						  :-------------------------:|:-------------------------:
![Splash Screen](https://bitbucket.org/Ras15/lis4331/raw/a60f3cf23accd9f5aa1070cff7a393157061031a/p2/img/Screen%20Shot%202019-04-25%20at%202.43.08%20PM.png) | ![TaskList](https://bitbucket.org/Ras15/lis4331/raw/3caed04cdef1f2e46ea0881c2789ab2d6d389d9f/p2/img/Screen%20Shot%202019-04-24%20at%206.25.12%20PM.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
