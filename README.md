# LIS4331 - Advanced Mobile Applications Development

## Remy Santos 

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My Assignment 1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App and Contacts App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide Git command descriptions

2. [A2 README.md](a2/README.md "My Assignment 2 README.md file")
    - Create Tip Calculator Android App
    - Provide screenshots of completed app
    - Provie Git command descriptions

3. [A3 README.md](a3/README.md "My Assignment 3 README.md file")
    - Create Android Currency Conversion App
    - Provide screenshots of application running
    - Provie Git command descriptions

4. [A4 README.md](a4/README.md "My Assignment 4 README.md file")
    - Create Android Mortgage Interest Calculator
    - Provide Screenshots of application running
    - Provide Git command descriptions
    
5. [A5 README.md](a5/README.md "My Assignment 5 README.md file")
    - Create Android News Reader Application
    - Provide Screenshots of application running
    - Provide Git command descriptions
    
6. [P1 README.md](p1/README.md "My Project 1 README.md file")
    - Create Music Android App
    - Provide Screenshots of completed app
    - Provide Git command descriptions
    
7. [P2 README.md](p2/README.md "My Project 2 README.md file")
    - Create Task List Android App
    - Provide Screenshots of completed app
    - Provide Git command descriptions

